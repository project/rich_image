<?php

namespace Drupal\rich_image;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Defines an interface for the rich_image field item.
 */
interface RichImageItemInterface extends FieldItemInterface {

  /**
   * Specifies whether the field supports only internal URLs.
   */
  const LINK_INTERNAL = 0x01;

  /**
   * Specifies whether the field supports only external URLs.
   */
  const LINK_EXTERNAL = 0x10;

  /**
   * Specifies whether the field supports both internal and external URLs.
   */
  const LINK_GENERIC = 0x11;

}
