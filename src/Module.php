<?php

namespace Drupal\rich_image;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\devutil\DevUtil;

/**
 * Provides constants and short helper methods for the module.
 */
class Module {
  /**
   * Machine name of the module.
   *
   * @var string NAME
   */
  const NAME = 'rich_image';

  /**
   * Human-readable label of the module.
   *
   * @var string LABEL
   */
  const LABEL = 'Rich Image';

  /**
   * Module webpage.
   *
   * @var string MODULE_WEBPAGE
   */
  const MODULE_WEBPAGE = 'https://www.drupal.org/project/' . self::NAME;

  /**
   * Writes variable values to a static debug file.
   *
   * Only works if \Drupal\devutil\DevUtil exists, otherwise silently bypasses.
   * This helps in debugging, but even if debug statements accidentally
   * remain in the code, it won't be a problem.
   */
  public static function debug($var, ...$params) {
    if (class_exists('\Drupal\devutil\DevUtil')) {
      DevUtil::debug($var, $params);
    }
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * The following two forms of URIs are transformed:
   * - 'entity:' URIs: to entity autocomplete ("label (entity id)") strings;
   * - 'internal:' URIs: the scheme is stripped.
   *
   * This method is the inverse of string2uri().
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *   The displayable string.
   *
   * @see string2uri()
   */
  public static function uri2string($uri) {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string may be chosen in case of the 'internal:'
    // or 'entity:' built-in schemes.
    if ($scheme === 'internal') {
      $uri_reference = explode(':', $uri, 2)[1];

      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      $path = parse_url($uri, PHP_URL_PATH);
      if ($path === '/') {
        $uri_reference = '<front>' . substr($uri_reference, 1);
      }

      $displayable_string = $uri_reference;
    }
    elseif ($scheme === 'entity') {
      $entityTypeManager = \Drupal::service('entity_type.manager');

      list($entity_type, $entity_id) = explode('/', substr($uri, 7), 2);
      // Show the 'entity:' URI as the entity autocomplete would.
      // @todo Support entity types other than 'node'. Will be fixed in
      //   https://www.drupal.org/node/2423093.
      if ($entity_type == 'node' and $entity = $entityTypeManager->getStorage($entity_type)->load($entity_id)) {
        $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
      }
    }

    return $displayable_string;
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * The following two forms of input are mapped to URIs:
   * - entity autocomplete ("label (entity id)") strings: to 'entity:' URIs;
   * - strings without a detectable scheme: to 'internal:' URIs.
   *
   * This method is the inverse of uri2string().
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   *
   * @see uri2string()
   */
  public static function string2uri($string) {
    // By default, assume the entered string is an URI.
    $uri = $string;

    // Detect entity autocomplete string, map to 'entity:' URI.
    $entity_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($string);
    if ($entity_id !== NULL) {
      // @todo Support entity types other than 'node'. Will be fixed in
      //   https://www.drupal.org/node/2423093.
      $uri = 'entity:node/' . $entity_id;
    }
    // Detect a schemeless string, map to 'internal:' URI.
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      // - '<front>' -> '/'
      // - '<front>#foo' -> '/#foo'
      if (strpos($string, '<front>') === 0) {
        $string = '/' . substr($string, strlen('<front>'));
      }
      $uri = 'internal:' . $string;
    }

    return $uri;
  }

  /**
   * Gets common values used across multiple methods in this class.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which common values are to be retrieved.
   * @param string $field_name
   *   The field name for which common values are to be retrieved.
   *
   * @return array
   *   The common values used across the class.
   */
  public static function expandEntityField(EntityInterface $entity, $field_name) {
    $static = &drupal_static(__METHOD__, []);
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $entity_revisionable = $entity instanceof RevisionableInterface;
    $revision_id = $entity->getRevisionId();
    if (empty($revision_id) and $entity_revisionable) {
      $revision_id = $entity_id;
    }

    $static_key = "${entity_type}:${entity_id}:${revision_id}:${field_name}";

    if (!isset($static[$static_key])) {

      /** @var \Drupal\Core\Field\FieldItemListInterface $field */
      $field = $entity->get($field_name);

      $static[$static_key] = [
        'bundle' => $entity->bundle(),
        'field_settings' => $field->getSettings(),
        'field_type' => $field->getFieldDefinition()->getType(),
        'entity_revisionable' => $entity_revisionable,
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'is_default_revision' => (!$entity_revisionable or ($entity_revisionable and $entity->isDefaultRevision())),
        'langcode' => $entity->language()->getId(),
        'revision_id' => $revision_id,
      ];
    }

    return $static[$static_key];
  }

}
