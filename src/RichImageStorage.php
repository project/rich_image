<?php

namespace Drupal\rich_image;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;

/**
 * Storage controller class for rich_image.
 */
class RichImageStorage {

  /**
   * The Cache Backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Cache Tags Invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The name of the data table.
   *
   * @var string
   */
  protected $tableBase = Module::NAME;

  /**
   * The name of the revision table.
   *
   * @var string
   */
  protected $tableRevision = Module::NAME . '_revision';

  /**
   * AbstractService constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The Cache Backend.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The Database.
   * @param \Drupal\Core\Database\Connection $database
   *   The Database.
   */
  public function __construct(
        CacheBackendInterface $cache_backend,
        CacheTagsInvalidatorInterface $cache_tags_invalidator,
        Connection $database
    ) {
    $this->cacheBackend = $cache_backend;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
    $this->database = $database;
  }

  /**
   * Check if attributes are already defined for the specified arguments.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be retrieved.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   * @param int $delta
   *   The delta of the image field.
   *
   * @return bool
   *   True if exists, else False.
   */
  public function exists(EntityInterface $entity, $field_name, $delta) {
    return (!empty(self::get($entity, $field_name, $delta))) ? TRUE : FALSE;
  }

  /**
   * Get attributes from the database for the specified arguments.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be retrieved.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   * @param int $delta
   *   The delta of the image field.
   *
   * @return array
   *   Properties array
   *   - attribution: The attribution text.
   *   - caption_title: The caption title text.
   *   - caption: The caption text.
   *   - caption_format: The caption format.
   */
  public function getRevision(EntityInterface $entity, $field_name, $delta) {
    $static = &drupal_static(__METHOD__, []);

    $expanded = Module::expandEntityField($entity, $field_name);

    /* if ($expanded['field_type'] !== 'image') {
    return [];
    } */

    $cacheKey = $this->getCacheKey(
      $expanded['entity_type'],
      $expanded['entity_id'],
      $expanded['revision_id'],
      $expanded['langcode'],
      $field_name,
      $delta);

    if (isset($static[$cacheKey])) {
      $rich_properties = $static[$cacheKey];
    }
    elseif ($cached = $this->cacheBackend->get($cacheKey)) {
      $rich_properties = $cached->data;
    }
    else {
      $select_fields = [
        'attribution',
        'caption_title',
        'caption',
        'caption_format',
        'link',
      ];
      $query = $this->database->select(
        $expanded['is_default_revision'] ? $this->tableBase : $this->tableRevision,
        'ri');
      $rich_properties = $query->fields('ri', $select_fields)
        ->condition('entity_type', $expanded['entity_type'], '=')
        ->condition('bundle', $expanded['bundle'], '=')
        ->condition('field_name', $field_name, '=')
        ->condition('entity_id', $expanded['entity_id'], '=')
        ->condition('revision_id', $expanded['revision_id'], '=')
        ->condition('langcode', $expanded['langcode'], '=')
        ->condition('delta', $delta, '=')
        ->execute()
        ->fetchAssoc();

      if (empty($rich_properties)) {
        $rich_properties = [
          'attribution' => '',
          'caption_title' => '',
          'caption' => '',
          'caption_format' => '',
          'link' => '',
        ];
      }

      // Let the cache depends on the entity.
      // @todo Use getCacheTags() to get the default list.
      $this->cacheBackend->set(
        $cacheKey,
        $rich_properties,
        Cache::PERMANENT,
        [
          $field_name,
          'rich_image',
        ]
      );
    }

    return $rich_properties;
  }

  /**
   * Insert rich attributes for all deltas of the entity and field_name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be inserted.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   * @param array $deltas_properties
   *   The array of all delta properties of the field.
   */
  public function insertRevisionDeltas(EntityInterface $entity, $field_name, array $deltas_properties) {
    $expanded = Module::expandEntityField($entity, $field_name);

    // Before inserting, delete all existing rows for this entity+revision_id
    // +field_name in the base and revision table.
    $this->deleteBase($entity, $field_name);

    if (!(empty($expanded['field_settings']['rich_image']['attribution_field'])
      and empty($expanded['field_settings']['rich_image']['caption_title_field'])
      and empty($expanded['field_settings']['rich_image']['caption_field'])
      and empty($expanded['field_settings']['rich_image']['link_field']))) {

      foreach ($deltas_properties as $delta => $properties) {
        $insert_fields = [
          'entity_type' => $expanded['entity_type'],
          'bundle' => $expanded['bundle'],
          'field_name' => $field_name,
          'entity_id' => $expanded['entity_id'],
          'revision_id' => $expanded['revision_id'],
          'langcode' => $expanded['langcode'],
          'delta' => $delta,
          'attribution' => (empty($expanded['field_settings']['rich_image']['attribution_field']) ? '' : $properties['attribution']),
          'caption_title' => (empty($expanded['field_settings']['rich_image']['caption_title_field']) ? '' : $properties['caption_title']),
          'caption' => (empty($expanded['field_settings']['rich_image']['caption_field']) ? '' : $properties['caption']),
          'caption_format' => (empty($expanded['field_settings']['rich_image']['caption_field']) ? '' : $properties['caption_format']),
          'link' => (empty($expanded['field_settings']['rich_image']['link_field']) ? '' : $properties['link']),
        ];

        $query = $this->database->insert($this->tableBase)->fields($insert_fields)->execute();

        // If entity type is revisionable, then insert to revision table also.
        if ($expanded['entity_revisionable']) {
          $query = $this->database->insert($this->tableRevision)->fields($insert_fields)->execute();
        }
      }
    }

    $this->clearCache($field_name);
  }

  /**
   * Delete all deltas of a base.
   *
   * Delete:
   * - from base table
   *     for entity_type+entity_id+langcode+field_name
   *     irrespective of revision_id.
   * - from revision table
   *     for entity_type+entity_id+langcode+field_name+revision_id.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be deleted.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   */
  public function deleteBase(EntityInterface $entity, $field_name) {
    $expanded = Module::expandEntityField($entity, $field_name);

    $query = $this->database->delete($this->tableBase);
    $query->condition('entity_type', $expanded['entity_type'], '=')
      ->condition('entity_id', $expanded['entity_id'], '=')
      ->condition('langcode', $expanded['langcode'], '=')
      ->condition('field_name', $field_name, '=')
      // ->condition('bundle', $expanded['bundle'], '=')
      ->execute();

    $this->deleteRevision($entity, $field_name);
  }

  /**
   * Delete all deltas of a revision.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be deleted.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   */
  public function deleteRevision(EntityInterface $entity, $field_name) {
    $expanded = Module::expandEntityField($entity, $field_name);

    if ($expanded['entity_revisionable']) {
      $query = $this->database->delete($this->tableRevision);
      $query->condition('entity_type', $expanded['entity_type'], '=')
        ->condition('entity_id', $expanded['entity_id'], '=')
        ->condition('revision_id', $expanded['revision_id'], '=')
        ->condition('langcode', $expanded['langcode'], '=')
        ->condition('field_name', $field_name, '=')
        // ->condition('bundle', $expanded['bundle'], '=')
        ->execute();
    }

    $this->clearCache($field_name);
  }

  /**
   * Delete attributes row from the database for the entity+field_name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The image entity for which rich attributes are to be deleted.
   * @param string $field_name
   *   The field name of the image field, like 'field_image' or
   *   'field_article_image'.
   */
  public function deleteEntity(EntityInterface $entity, $field_name) {
    $expanded = Module::expandEntityField($entity, $field_name);

    $query = $this->database->delete($this->tableBase);
    $query->condition('entity_type', $expanded['entity_type'], '=')
      ->condition('entity_id', $expanded['entity_id'], '=')
      // ->condition('langcode', $expanded['langcode'], '=')
      ->condition('field_name', $field_name, '=')
      // ->condition('bundle', $expanded['bundle'], '=')
      ->execute();

    if ($expanded['entity_revisionable']) {
      $query = $this->database->delete($this->tableRevision);
      $query->condition('entity_type', $expanded['entity_type'], '=')
        ->condition('entity_id', $expanded['entity_id'], '=')
        // ->condition('langcode', $expanded['langcode'], '=')
        ->condition('field_name', $field_name, '=')
        // ->condition('bundle', $expanded['bundle'], '=')
        ->execute();
    }
    $this->clearCache($field_name);
    // @todo Try to return the count of the affected rows.
  }

  /**
   * Clears the cache for a certain field name.
   *
   * @param string $field_name
   *   The field name of the image field, like 'field_image'
   *   or 'field_article_image'.
   */
  public function clearCache($field_name) {
    $this->cacheTagsInvalidator->invalidateTags([
      $field_name,
      'rich_image',
    ]);
  }

  /**
   * Constructs the cache key.
   *
   * @param string $entity_type
   *   The entity type, like 'node' or 'comment'.
   * @param int $entity_id
   *   The entity id.
   * @param int $revision_id
   *   The revision id.
   * @param string $langcode
   *   The langcode key, like 'en' or 'fr'.
   * @param string $field_name
   *   The field name of the image field, like 'field_image'
   *   or 'field_article_image'.
   * @param int $delta
   *   The delta of the image field.
   *
   * @return string
   *   The ':' delimited concatenation of input.
   */
  public function getCacheKey($entity_type, $entity_id, $revision_id, $langcode, $field_name, $delta) {
    return implode(
        ":",
        [
          'rich_image',
          $entity_type,
          $entity_id,
          $revision_id,
          $langcode,
          $field_name,
          $delta,
        ]
    );
  }

}
