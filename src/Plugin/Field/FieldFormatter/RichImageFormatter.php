<?php

namespace Drupal\rich_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\rich_image\Module;

/**
 * Plugin implementation of the 'rich_image' formatter.
 *
 * @FieldFormatter(
 *   id = "rich_image",
 *   label = @Translation("Rich Image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class RichImageFormatter extends ImageFormatter {

  const PREPEND_TEXT = '[' . Module::LABEL . '] ';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rich_image' => [
        'attribution_render' => TRUE,
        'caption_title_render' => TRUE,
        'caption_render' => TRUE,
        'render_at_top' => FALSE,
        'link_image_render' => TRUE,
        'link_properties_render' => TRUE,
        'link_target' => TRUE,
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    // Removing base image's link settings, since it overlaps/confuses user
    // with Rich Image link field.
    unset($element['image_link']);

    $field_settings = $this->getFieldSettings();
    $rich_image_settings = $this->getSetting('rich_image');
    $field_name = $this->fieldDefinition->getName();

    $element['rich_image'] = [
      '#type' => 'details',
      '#title' => t('Rich Image Properties'),
      '#description' => t('Powered by <em>@module_label</em> module.', ['@module_label' => Module::LABEL]),
      '#open' => TRUE,
      '#weight' => 20,
    ];

    if (!empty($field_settings['rich_image']['attribution_field'])) {
      $element['rich_image']['attribution_render'] = [
        '#type' => 'checkbox',
        '#title' => t('Render <em>Attribution</em> property'),
        '#default_value' => $rich_image_settings['attribution_render'],
        '#weight' => 1,
      ];
    }
    if (!empty($field_settings['rich_image']['caption_title_field'])) {
      $element['rich_image']['caption_title_render'] = [
        '#type' => 'checkbox',
        '#title' => t('Render <em>Caption Title</em> property'),
        '#default_value' => $rich_image_settings['caption_title_render'],
        '#weight' => 2,
      ];
    }
    if (!empty($field_settings['rich_image']['caption_field'])) {
      $element['rich_image']['caption_render'] = [
        '#type' => 'checkbox',
        '#title' => t('Render <em>Caption</em> property'),
        '#default_value' => $rich_image_settings['caption_render'],
        '#weight' => 3,
      ];
    }
    if (!(empty($field_settings['rich_image']['caption_field'])
      and empty($field_settings['rich_image']['caption_field'])
      and empty($field_settings['rich_image']['caption_field']))) {
      $element['rich_image']['render_at_top'] = [
        '#type' => 'checkbox',
        '#title' => t('Render at the top'),
        '#default_value' => $rich_image_settings['render_at_top'],
        '#weight' => 3,
      ];
    }
    if (!empty($field_settings['rich_image']['link_field'])) {
      $element['rich_image']['hr_1'] = [
        '#type' => 'item',
        '#description' => '<hr>',
        '#weight' => 4,
      ];
      $element['rich_image']['link_image_render'] = [
        '#type' => 'checkbox',
        '#title' => t('Link image to <em>Link</em> property'),
        '#default_value' => $rich_image_settings['link_image_render'],
        '#weight' => 5,
      ];
      $element['rich_image']['link_properties_render'] = [
        '#type' => 'checkbox',
        '#title' => t('Link properties to <em>Link</em> property'),
        '#default_value' => $rich_image_settings['link_properties_render'],
        '#weight' => 6,
      ];
      $element['rich_image']['link_target'] = [
        '#type' => 'checkbox',
        '#title' => t('Open link in a new window'),
        '#default_value' => $rich_image_settings['link_target'],
        '#return_value' => '_blank',
        '#weight' => 7,
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $render_settings = $this->getRenderSettings();

    $render = [];
    if (!empty($render_settings['attribution_render'])) {
      $render[] = t('Attribution');
    }
    if (!empty($render_settings['caption_title_render'])) {
      $render[] = t('Caption Title');
    }
    if (!empty($render_settings['caption_render'])) {
      $render[] = t('Caption');
    }
    if (!empty($render)) {
      $render = implode(', ', $render);
      $summary[] = $this::PREPEND_TEXT . t('Render: @ph1', ['@ph1' => $render])
        . (!empty($render_settings['render_at_top']) ? (' (' . t('At the top') . ')') : '');
    }

    if (!(empty($render_settings['link_image_render'])
        and empty($render_settings['link_properties_render']))) {
      $linked = [];
      if (!empty($render_settings['link_image_render'])) {
        $linked[] = t('Image');
      }
      if (!empty($render_settings['link_properties_render'])) {
        $linked[] = t('Properties');
      }
      $summary[] = $this::PREPEND_TEXT . t('Link:') . ' '
        . implode(', ', $linked) . ' ('
        . (empty($this->getSetting('rich_image')['link_target'])
          ? t('Open in the same window') : t('Open in a new window'))
        . ')';
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as $delta => $element) {
      // Set a new theme callback function for the image caption formatter.
      $elements[$delta]['#theme'] = 'rich_image_formatter';
      $elements[$delta]['#rich_image_formatter_settings'] = $this->getRenderSettings();
    }

    return $elements;
  }

  /**
   * Override ImageFormatterBase::getEntitiesToView().
   *
   * Because there is too much code that is overridden, I decided to write a
   * patch in RichImageItem::serValue.
   */
  protected function getEntitiesToViewNotUsed(EntityReferenceFieldItemListInterface $items, $langcode) {
    // Add the default image if needed.
    if ($items->isEmpty()) {
      $default_image = $this->getFieldSetting('default_image');
      // If we are dealing with a configurable field, look in both
      // instance-level and field-level settings.
      if (empty($default_image['uuid']) && $this->fieldDefinition instanceof FieldConfigInterface) {
        $default_image = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('default_image');
      }
      if (!empty($default_image['uuid']) && $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $default_image['uuid'])) {
        // Clone the FieldItemList into a runtime-only object for the formatter,
        // so that the fallback image can be rendered without affecting the
        // field values in the entity being rendered.
        $items = clone $items;
        $items->setValue([
          'target_id' => $file->id(),
          'alt' => $default_image['alt'],
          'title' => $default_image['title'],
          'width' => $default_image['width'],
          'height' => $default_image['height'],
          'attribution' => $default_image['attribution'],
          'entity' => $file,
          '_loaded' => TRUE,
          '_is_default' => TRUE,
        ]);
        $file->_referringItem = $items[0];
      }
    }

    return parent::getEntitiesToView($items, $langcode);
  }

  /**
   * Combines field+formatter settings for a propert to render.
   *
   * @return array
   *   Array of boolean flags whether the properties should be rendered.
   */
  private function getRenderSettings() {
    $static = &drupal_static(__METHOD__, []);

    $field_name = $this->fieldDefinition->getName();

    if (empty($static[$field_name])) {
      $field_settings = $this->getFieldSettings();
      $rich_image_settings = $this->getSetting('rich_image');

      $static[$field_name]['attribution_render'] = !(empty($field_settings['rich_image']['attribution_field'])
        or empty($rich_image_settings['attribution_render']));
      $static[$field_name]['caption_title_render'] = !(empty($field_settings['rich_image']['caption_title_field'])
        or empty($rich_image_settings['caption_title_render']));
      $static[$field_name]['caption_render'] = !(empty($field_settings['rich_image']['caption_field'])
        or empty($rich_image_settings['caption_render']));
      $static[$field_name]['render_at_top'] = $rich_image_settings['render_at_top'];

      $static[$field_name]['link_image_render'] = !(empty($field_settings['rich_image']['link_field'])
        or empty($rich_image_settings['link_image_render']));
      $static[$field_name]['link_properties_render'] = !(empty($field_settings['rich_image']['link_field'])
        or empty($rich_image_settings['link_properties_render']));
      $static[$field_name]['link_target'] = $rich_image_settings['link_target'];
    }

    return $static[$field_name];
  }

}
