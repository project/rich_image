<?php

namespace Drupal\rich_image\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\field\FieldConfigInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\rich_image\Module;
use Drupal\rich_image\RichImageItemInterface;

/**
 * Extends ImageItem base class to add new attributes.
 */
class RichImageItem extends ImageItem implements RichImageItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings_default = [
      'default_image' => [
        'attribution' => '',
        'caption_title' => '',
        'caption_formatted' => [
          'value' => '',
          'format' => 'restricted_html',
        ],
      ],
    ];
    return array_replace_recursive($settings_default, parent::defaultStorageSettings());
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings_default = [
      'default_image' => [
        'attribution' => '',
        'caption_title' => '',
        'caption_formatted' => [
          'value' => '',
          'format' => 'restricted_html',
        ],
      ],
      'rich_image' => [
        'attribution_field' => FALSE,
        'attribution_field_required' => FALSE,
        'caption_title_field' => FALSE,
        'caption_title_field_required' => FALSE,
        'caption_field' => FALSE,
        'caption_field_required' => FALSE,
        'link_field' => FALSE,
        'link_field_required' => FALSE,
        'link_type' => static::LINK_GENERIC,
      ],
    ];

    return array_replace_recursive($settings_default, parent::defaultFieldSettings());
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['attribution'] = DataDefinition::create('string')
      ->setLabel(t('Attribution'))
      ->setDescription(t('Image attribution text, giving credits for the image.'));

    $properties['caption_title'] = DataDefinition::create('string')
      ->setLabel(t('Caption Title'))
      ->setDescription(t('Image caption title text.'));

    $properties['caption'] = DataDefinition::create('string')
      ->setLabel(t('Caption'))
      ->setDescription(t('Image caption text.'));

    $properties['link'] = DataDefinition::create('string')
      ->setLabel(t('Link URL'))
      ->setDescription(t('Image link URL.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    // Get base form from ImageItem.
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = $this->getSettings();

    // Get the default field settings.
    $settings_default = self::defaultFieldSettings();

    $element['rich_image'] = [
      '#type' => 'details',
      '#title' => $this->t('Rich Image Properties'),
      '#description' => $this->t('Powered by <em>@module_label</em> module.', ['@module_label' => Module::LABEL]),
      '#open' => TRUE,
      '#weight' => 20,
    ];

    // Add attribution option.
    $element['rich_image']['attribution_field'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Attribution</em> property'),
      '#default_value' => isset($settings['rich_image']['attribution_field']) ? $settings['rich_image']['attribution_field'] : $settings_default['rich_image']['attribution_field'],
      '#description' => t('Text field for image attribution.'),
      '#weight' => 1,
    ];

    $element['rich_image']['attribution_field_required'] = [
      '#type' => 'checkbox',
      '#title' => t('<em>Attribution</em> property required'),
      '#default_value' => isset($settings['rich_image']['attribution_field_required']) ? $settings['rich_image']['attribution_field_required'] : $settings_default['rich_image']['attribution_field_required'],
      '#description' => '',
      '#states' => [
        'visible' => [
          ':input[name="settings[rich_image][attribution_field]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 2,
    ];

    // Add caption_title option.
    $element['rich_image']['caption_title_field'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Caption Title</em> property'),
      '#default_value' => isset($settings['rich_image']['caption_title_field']) ? $settings['rich_image']['caption_title_field'] : $settings_default['rich_image']['caption_title_field'],
      '#description' => t('Text field for image caption title.'),
      '#weight' => 3,
    ];

    $element['rich_image']['caption_title_field_required'] = [
      '#type' => 'checkbox',
      '#title' => t('<em>Caption Title</em> property required'),
      '#default_value' => isset($settings['rich_image']['caption_title_field_required']) ? $settings['rich_image']['caption_title_field_required'] : $settings_default['rich_image']['caption_title_field_required'],
      '#description' => '',
      '#states' => [
        'visible' => [
          ':input[name="settings[rich_image][caption_title_field]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 4,
    ];

    // Add caption option.
    $element['rich_image']['caption_field'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Caption</em> property'),
      '#default_value' => isset($settings['rich_image']['caption_field']) ? $settings['rich_image']['caption_field'] : $settings_default['rich_image']['caption_field'],
      '#description' => t('Text field for image caption.'),
      '#weight' => 5,
    ];
    $element['rich_image']['caption_field_required'] = [
      '#type' => 'checkbox',
      '#title' => t('<em>Caption</em> property required'),
      '#default_value' => isset($settings['rich_image']['caption_field_required']) ? $settings['rich_image']['caption_field_required'] : $settings_default['rich_image']['caption_field_required'],
      '#description' => '',
      '#states' => [
        'visible' => [
          ':input[name="settings[rich_image][caption_field]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 6,
    ];

    // Add link option.
    $element['rich_image']['link_field'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Link</em> property'),
      '#default_value' => isset($settings['rich_image']['link_field']) ? $settings['rich_image']['link_field'] : $settings_default['rich_image']['link_field'],
      '#description' => t('Link field for image.'),
      '#weight' => 7,
    ];
    $element['rich_image']['link_field_required'] = [
      '#type' => 'checkbox',
      '#title' => t('<em>Link</em> property required'),
      '#default_value' => isset($settings['rich_image']['link_field_required']) ? $settings['rich_image']['link_field_required'] : $settings_default['rich_image']['link_field_required'],
      '#description' => '',
      '#states' => [
        'visible' => [
          ':input[name="settings[rich_image][link_field]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 8,
    ];
    $element['rich_image']['link_type'] = [
      '#type' => 'radios',
      '#title' => t('Allowed link type'),
      '#default_value' => isset($settings['rich_image']['link_type']) ? $settings['rich_image']['link_type'] : $settings_default['rich_image']['link_type'],
      '#options' => [
        static::LINK_INTERNAL => t('Internal links only'),
        static::LINK_EXTERNAL => t('External links only'),
        static::LINK_GENERIC => t('Both internal and external links'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="settings[rich_image][link_field]"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 9,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultImageForm(array &$element, array $settings) {
    parent::defaultImageForm($element, $settings);

    $element['default_image']['attribution'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attribution'),
      '#description' => $this->t('Powered by <em>@module_label</em> module.', ['@module_label' => Module::LABEL]),
      '#default_value' => $settings['default_image']['attribution'],
      '#maxlength' => 64,
      '#size' => 60,
      '#weight' => 21,
    ];

    $element['default_image']['caption_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Caption Title'),
      '#description' => $this->t('Powered by <em>@module_label</em> module.', ['@module_label' => Module::LABEL]),
      '#default_value' => $settings['default_image']['caption_title'],
      '#maxlength' => 255,
      '#size' => 60,
      '#weight' => 22,
    ];

    $element['default_image']['caption_formatted'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Caption'),
      '#description' => $this->t('Powered by <em>@module_label</em> module.', ['@module_label' => Module::LABEL]),
      '#default_value' => $settings['default_image']['caption_formatted']['value'],
      '#format' => $settings['default_image']['caption_formatted']['format'],
      '#rows' => 2,
      '#weight' => 23,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * When an entity is inserted or updated from a form, because the rich
   * properties are in a tree (rich_image.attribution,
   * rich_image.caption_title, etc, by the time it reaches here (before even
   * the insertion happens, i.e. rich_image_entity_storage_load is not called
   * yet), it needs to be moved up in hierarchy.
   */
  public function getValue() {
    $parent_properties = parent::getValue();

    $rich_properties = [];

    if (isset($parent_properties['rich_image'])) {
      $rich_properties = $parent_properties['rich_image'];
      unset($parent_properties['rich_image']);

      $rich_properties['caption'] = $rich_properties['caption_formatted']['value'];
      $rich_properties['caption_format'] = $rich_properties['caption_formatted']['format'];
      unset($rich_properties['caption_formatted']);
    }

    /* elseif (!empty($parent_properties['_is_default'])) {
    $field_settings = $this->getSettings();
    $rich_properties['attribution'] =
    $field_settings['default_image']['attribution'];
    $rich_properties['caption_title'] =
    $field_settings['default_image']['caption_title'];
    $rich_properties['caption'] =
    $field_settings['default_image']['caption_formatted']['value'];
    } */

    // This code block is taken care of in rich_image_entity_storage_load. It
    // had to be done there as revert revision does not work if done only only
    // here.
    /* if (!isset($parent_properties['attribution'])) {
    $richImageStorage = \Drupal::service('rich_image.storage');
    $rich_properties = $richImageStorage->getRevision(
    $this->getEntity(),
    $this->getParent()->getName(),
    $this->getName());
    } */

    return $rich_properties + $parent_properties;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // This method is called many times:
    // - when entity edit form is submitted
    // - when entity is viewed
    // It has become quite problematic to handle the rich_image hierarchy
    // on the field edit and field settings form. Consider removing this
    // hierarchy.
    /* if (isset($values['rich_image'])) {
    $values = $values + $values['rich_image'];
    unset($values['rich_image']);
    }

    if (!isset($values['attribution']) and $values['alt'] == 'a1') {
    $values['attribution'] = '';
    $values['caption_title'] = '';
    $values['caption'] = '';
    $values['caption_format'] = 'restricted_html';
    $values['link'] = '';
    } */

    // When this method is called from ImageFormatterBase::getEntitiesToView(),
    // it is when the default image is to be loaded. Because there is too much
    // code in that method, and I want to avoid copying the whole method into
    // an overriding method - it has a risk that something will change in
    // future Drupal versions and break overwritten code. Hence this patch is
    // written.
    if (!empty($values['_is_default'])) {
      $default_image = $this->getSetting('default_image');
      if (empty($default_image['uuid'])
        and ($field_definition = $this->getFieldDefinition()) instanceof FieldConfigInterface) {
        $default_image = $field_definition->getFieldStorageDefinition()->getSetting('default_image');
      }

      $values['attribution'] = $default_image['attribution'];
      $values['caption_title'] = $default_image['caption_title'];
      $values['caption'] = $default_image['caption_formatted']['value'];
      $values['link'] = '';
    }

    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    if ($this->entity and $this->entity instanceof EntityInterface) {
      $values = $this->getValue();
      if (isset($values['link']) and !empty($values['link'])) {
        $values['link'] = Module::string2uri($values['link']);
        $this->setValue($values);
      }
    }
    else {
      trigger_error(sprintf("Missing file with ID %s.", $this->target_id), E_USER_WARNING);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();

    return [
      'attribution' => $random->sentences(4),
      'caption_title' => $random->sentences(4),
      'caption' => $random->sentences(8),
    ] + parent::generateSampleValue($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision() {
    $richImageStorage = \Drupal::service('rich_image.storage');

    $richImageStorage->deleteRevision($this->getEntity(), $this->getParent()->getName());

    parent::deleteRevision();
  }

}
