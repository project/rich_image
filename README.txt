CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Known Issues
 * Troubleshooting
 * Developers and Maintainers
 * Credits
 * Postscript


INTRODUCTION
------------

The Rich Image module enriches the core Image field with the following new
properties:
 * Attribution (aka Image Credits, Copyright notice, etc.) - Plain Text
 * Caption Title - Plain Text
 * Caption (Formatted Text with Caption Format)
 * Link (with Link Target)

The module extends the ImageItem class, and adds the rich properties.
The module is designed so that it creates its own tables, instead of using
Image field tables (so that if any changes happen to the core Image field in
future, this module remains independent, and that enabling and disabling this
module has no effect on existing tables).

The Image field is extended fully, irrespective of in which Entity Type it is
used.


REQUIREMENTS
------------

This module requires the following:

 * Drupal Core - 8.8+ or 9.0+
 * Image - Drupal Core Module


RECOMMENDED MODULES
-------------------

None.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

When you add a new Image field to any Entity Bundle, in the Manage Fields, the
following configuration is made available:
 * Options to enable the new rich properties for that bundle.
 * The new properties can also be made "required" for that bundle.
 * Default value for some properties can be set.

Note: Default values for all new properties are not made available, because it
did not make sense. For e.g. Default value for Caption Title, Caption, Link and
Link Target did not make sense to the module author.

On the add/edit content page, the new properties will now appear.


KNOWN ISSUES
------------

Though there are no known issues, Revision management works fine. The following
aspects can be made better:
 1. Cache management.
 2. Performance.


TROUBLESHOOTING
---------------

Q: Sample Question?
A: Sample Answer.


DEVELOPERS AND MAINTAINERS
--------------------------

Original Developer:

 * Aalap Shah (fishfin) - https://www.drupal.org/u/fishfin

Current maintainers:

 * Aalap Shah (fishfin) - https://www.drupal.org/u/fishfin


CREDITS
-------

Credits to the following contributed modules for getting the developer started
on writing this module:

 * Image Field Caption - https://www.drupal.org/project/field_image_caption

And the Nalanda Mahavihara library of knowledge shared by fellow developers on
Drupal.org and stackoverflow.com! THANK YOU!


POSTSCRIPT
----------

For a full description of the module, visit the project page:
https://www.drupal.org/project/rich_image

Made in India with <3 by fishfin. Created Sep, 2020.

"I'd made it this far and refused to give up,
 Because all my life I had always finished the race."
 - Louis Zamperini
